/*
Будут таблицы:
1) post - Для новостей ленты
2) user - Для пользоватлей
3) category - Для категорий новостей
4) likes - Для лайков
*/
-- Таблица пользователей
create table `users`
  (
  `id` int unsigned not null auto_increment primary key,
  `name` varchar(50) not null,
  `age` tinyint unsigned not null,
  `created_at` timestamp default current_timestamp,
  `updated_at` timestamp default current_timestamp on update current_timestamp
  )
default character set utf8 collate utf8_unicode_ci
-- Таблица категорий постов
create table `category`
  (
  `id` int unsigned not null auto_increment primary key,
  `name` varchar(100) not null
  )
default character set utf8 collate utf8_unicode_ci
alter table `category` add index `category_name_index`(`name`)
-- Таблица постов
create table `post`
  (
  `id` int unsigned not null auto_increment primary key,
  `user_id` int unsigned not null,
  `category_id` int unsigned,
  `text` varchar(243) not null,
  `created_at` timestamp default current_timestamp,
  `updated_at` timestamp default current_timestamp on update current_timestamp
  )
default character set utf8 collate utf8_unicode_ci
alter table `post` add index `post_user_id_index`(`user_id`)
alter table `post` add constraint `post_user_id_foreign` foreign key (`user_id`) references `users` (`id`) on delete cascade
alter table `post` add index `post_category_id_index`(`category_id`)
alter table `post` add constraint `post_category_id_foreign` foreign key (`category_id`) references `category` (`id`) on delete set null
-- Таблица лайков
create table `like`
  (
  `post_id` int unsigned not null,
  `user_id` int unsigned not null
  )
default character set utf8 collate utf8_unicode_ci
alter table `like` add index `like_user_id_index`(`user_id`)
alter table `like` add constraint `like_user_id_foreign` foreign key (`user_id`) references `users` (`id`) on delete cascade
alter table `like` add index `like_post_id_index`(`post_id`)
alter table `like` add constraint `like_post_id_foreign` foreign key (`post_id`) references `post` (`id`) on delete cascade
-- Запросы:
-- Создадим пользователя, категорию
INSERT INTO `users`
  (
    `name`,
    `age`
  )
VALUE ('Имя',18)
INSERT INTO `category`
  (
    `name`
  )
VALUE ('Наука')
-- 5. запрос на добавление поста в ленту.
INSERT INTO `post`
  (
    `user_id`,
    `category_id`,
    `text`
  )
VALUE (1,1,'PHP 7 версии быстрее 5 версии')
-- 1. запрос на постановку лайка от юзера к новости;
INSERT IGNORE INTO `like` VALUE (1,1)
-- 2. запрос на отмену лайка;
DELETE FROM `like` WHERE `post_id` = 1 AND `user_id` = 1
-- 3. выборка пользователей, оценивших новость, желательно учесть что их могут быть тысячи и сделать возможность постраничного вывода;
SELECT
  u.`*`
FROM
  `post` p
INNER JOIN
  `like` l
ON
  l.`post_id` = p.`id`
INNER JOIN
  `users` u
ON
  u.`id` = l.`user_id`
WHERE
  p.`id` = 1
LIMIT 0, 500
-- 4. запрос для вывода ленты новостей;
SELECT
  p.`*`
FROM
  `category` c
INNER JOIN
  `post` p
ON
  p.`category_id` = c.`id`
WHERE
  c.`name` = 'Наука'
LIMIT 0, 100