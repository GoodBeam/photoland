<?php

class FileReader implements SeekableIterator
{
    /** @var SplFileObject */
    private $file;

    public function __construct(string $fileName)
    {
        $this->file = new SplFileObject($fileName, 'rb');
    }

    public function current(): string
    {
        return $this->file->current();
    }

    public function seek($position): void
    {
        $this->file->seek($position);
    }

    public function key(): int
    {
        return $this->file->key();
    }

    public function rewind(): void
    {
        $this->file->rewind();
    }

    public function next(): void
    {
        $this->file->next();
    }

    public function valid(): bool
    {
        return $this->file->valid();
    }
}