/*
Для работы данного способа нужна дополнительная таблица с числами:
1) Она указывает максимальное количество email'ов в одной записи
2) С ее помощью разбиваем одну запись с несколькими email'ами на несколько по одному email'у в каждой записи
*/
CREATE TABLE numbers (n INT PRIMARY KEY);

INSERT INTO numbers VALUES (1),(2),(3),(4),(5),(6);
/*
А дальше агрегация данных:
Замеры производились на таблице с 1 000 000 записей
Таблица была создана командой из задания и не была модифицирована
На подсчет доменов в данной таблице ушло 4.84 секунды
То есть при подсчете таблицы размером ~ 100 млн записей уйдет ~ 500 секунд времени (8.3 минуты)
*/
SELECT
  COUNT(*),
  SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(users.email, ',', numbers.n), ',', -1), '@', -1)  emails
FROM
  numbers
INNER JOIN
  users
ON
   CHAR_LENGTH(users.email)-CHAR_LENGTH(REPLACE(users.email, ',', ''))>=numbers.n-1
GROUP BY emails;

